#!/bin/bash

xsv search -s UsageClass 'Physical' data/raw/Checkouts_by_Title.csv | xsv search -s MaterialType 'BOOK' > data/filtered/Checkouts_by_Title.csv
xsv select 'Title' data/filtered/Checkouts_by_Title.csv > data/filtered/Titles.csv
