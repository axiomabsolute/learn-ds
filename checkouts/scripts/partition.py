import csv
from collections import defaultdict

files = defaultdict(lambda: [])

with open("data/filtered/Checkouts_by_title.csv") as inf:
    reader = csv.reader(inf)
    counter = 0
    next(reader)
    for record in reader:
        title = record[6]
        filename = hash(title) % 1000
        files[filename].append(record)
        counter += 1
        if counter % 100000 == 0:
            for filename, records in files.items():
                with open(f"data/partitioned/{filename}.csv", "a") as outf:
                    writer = csv.writer(outf)
                    writer.writerows(records)
            files = defaultdict(lambda: [])
            print(f"Processed {counter} records")
print("Done.")