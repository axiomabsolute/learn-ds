import csv
from collections import Counter

with open("data/filtered/Titles.csv") as inf:
    reader = csv.DictReader(inf)
    unique_titles = Counter((record["Title"] for record in reader))

with open("data/filtered/Unique_Titles.txt", "w") as outf:
    for title,count in unique_titles.items():
        if count >= 4:
            outf.write(title)
            outf.write("\n")
