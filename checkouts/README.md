# Seattle Public Library Checkouts Data

This project explores various aspects of a [dataset containing monthly checkout numbers from titles in the Seattle public library system](https://data.seattle.gov/Community/Checkouts-by-Title/tmmm-ytt6).

All projects assume you've downloaded this data into the file `./data/raw/Checkouts_by_Title.csv`.

## Project: Seasonality

*Problem*: Can we identify titles with strong, seasonal checkout trends?

> *Skills Explored*: Signal processing methods

The hypothesis is that certain books have seasonal checkout trends, which may be used to notify browsers about popular seasonal books and recommend books which may be easier to access out-of-season.

To test this hypothesis, the `Explore.ipynb` notebook selects 10 books with heavy checkout history which contain the word "christmas" in their title, which we expect are more likely to have seasonal checkout trends around the Christmas holiday season. As a control, a second set of 10 books containing the word "dog" are selected, which we expect to act as a control group.

The notebook then uses the Power Spectral Density (PSD) function to generate a graph, visually indicating harmonic frequencies in the signal represented by the monthly checkout data. Further, the notebook explores some simple functions for detecting and classifying books which have seasonal checkout spikes around the Christmas holiday season.

### Dependencies

The project uses the tool [`xsv`](https://github.com/BurntSushi/xsv) to help pre-process the CSV data. See the project page for installation details.

### Preparing the Data

1. Run `./scripts/select_data.sh`
2. Run `poetry run python ./scripts/partition.py`
    - *Note*: this step will take some time to complete as it segments the data
3. Run `poetry run python ./scripts/unique_titles.py`

### Running notebooks

From the root of the `checkouts` sub-project:

1. Ensure the Python package and virtual environment management package `poetry` is installed
2. Run `poetry install`
3. Run `poetry run jupyter notebook`
4. Open `Explore.ipynb`
