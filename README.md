# Learn Data Science

This is a repository of projects that I've used to learn or practice various skills in the field of data science. Each sub-folder under the `src` directory is an independent, isolated project.

## Notes

- To the extent possible, each project will be isolated and self contained.
	+ Designed to run on *nix systems
	+ Dockerized, when possible
	+ Documented such that they are reproducible
	+ Use Make as the standard entrypoint
- Small data sets may be included. Medium data sets may be hosted and utilize a script for fetching data. Larger data sets may simply be referenced via a link.
- My primary programming language for data science is Python. For specific projects I may branch out and use other languages. I'm particularly interested in Julia.
